# koinos-rpc

Typescript library to communicate with Koinos API via RPC

# How to use

```typescript
import { Client } from 'koinos-rpc';

void (async () => {
   const client = new Client(['https://api.koinosblocks.com']);
   const { head_topology } = await client.chain.getHeadInfo();
   console.log(JSON.stringify(head_topology, null, 2));
})();
```

Visit `examples` directory for more examples.

# Tests
There are sets of unit and integration tests

Install dependencies:

```shell
yarn install
```

Run unit tests:

```shell
yarn run test:unit
```

Run integration tests:

```shell
yarn run test:integration
```

# Licence

MIT
