import { Client } from '../src';

void (async () => {
   const client = new Client(['https://api.koinosblocks.com']);

   const { topology } = await client.blockStore.getHighestBlock();

   const numberOfBlocks = 10;
   console.log(`Asking for ${numberOfBlocks} last blocks starting with id: ${topology.id}, height: ${topology.height}`);
   const { block_items } = await client.blockStore.getBlocksByHeight(topology.id, topology.height - numberOfBlocks, numberOfBlocks);

   console.log(`Received ${block_items.length} blocks:`);
   block_items.forEach((block) => {
      console.log(` * block id: ${block.block_id}, height: ${block.block_height}, timestamp: ${block.block!.header.timestamp}`);
   });
})();
