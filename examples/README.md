Install dependencies

```bash
yarn install
````

Run selected example with `ts-node`

```bash
ts-node getHeadInfo.ts
```
