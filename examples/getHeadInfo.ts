import { Client } from '../src';

void (async () => {
   const client = new Client(['https://api.koinosblocks.com']);
   const { head_topology } = await client.chain.getHeadInfo();
   console.log(JSON.stringify(head_topology, null, 2));
})();
