import { Client } from '../src';

void (async () => {
   const client = new Client(['https://api.koinosblocks.com']);

   for await (const block of client.blockStore.getBlocks()) {
      console.log(`Received block ${block.block_height} with id: ${block.block_id}, timestamp: ${block.block!.header.timestamp}`);
   }
})();
