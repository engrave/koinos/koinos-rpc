import { parse, Root } from 'protobufjs';
import * as bs58 from 'bs58';
import base64url from 'base64url';
import { Client } from '../Client';
import { tokenProto } from './proto';

interface TokenContractResponse {
   result: string;
}

const entryPoints = {
   balance: 0x5c721497,
   precision: 0xee80fd2f,
   symbol: 0xb76a7ca1
};

const root = Root.fromJSON(tokenProto);

export class Token {
   constructor(private readonly client: Client, private readonly contractId: string, private readonly precision: number, private readonly symbol: string) {}

   static async create(client: Client, contractId: string): Promise<Token> {
      const precision = await Token.getPrecision(client, contractId);
      const symbol = await Token.getSymbol(client, contractId);
      return new Token(client, contractId, precision, symbol);
   }

   private static encodeArgs(args: Record<string, any>, argumentType: string): string {
      const type = root.lookupType(argumentType);
      const message = type.create(args);
      const buffer = Buffer.from(type.encode(message).finish());
      return base64url.encode(buffer);
   }

   private static async call<T>(client: Client, contractId: string, entry_point: number, argument_type: string, result_tyoe: string, args: Record<string, any>): Promise<T> {
      const { result } = await client.call<TokenContractResponse, 'chain', string>('chain', 'read_contract', {
         args: Token.encodeArgs(args, argument_type),
         contract_id: contractId,
         entry_point
      });

      const buffer = base64url.toBuffer(result);
      const decoded = root.lookupType(result_tyoe).decode(buffer).toJSON();
      return decoded.value;
   }

   private static async getPrecision(client: Client, contractId: string): Promise<number> {
      return Token.call(client, contractId, entryPoints.precision, 'koinos.contracts.token.decimals_arguments', 'koinos.contracts.token.decimals_result', {});
   }

   private static async getSymbol(client: Client, contractId: string): Promise<string> {
      return Token.call(client, contractId, entryPoints.symbol, 'koinos.contracts.token.symbol_arguments', 'koinos.contracts.token.symbol_result', {});
   }

   private static async getBalance(client: Client, contractId: string, address: string): Promise<string> {
      return Token.call(client, contractId, entryPoints.balance, 'koinos.contracts.token.balance_of_arguments', 'koinos.contracts.token.balance_of_result', {
         owner: bs58.decode(address)
      });
   }

   async getPrecision(): Promise<number> {
      return Token.getPrecision(this.client, this.contractId);
   }

   async getSymbol(): Promise<string> {
      return Token.getSymbol(this.client, this.contractId);
   }

   async getBalance(address: string, raw = false): Promise<string> {
      const balance = await Token.getBalance(this.client, this.contractId, address);
      return raw ? balance : `${Number(balance) / 10 ** this.precision} ${this.symbol}`;
   }
}
