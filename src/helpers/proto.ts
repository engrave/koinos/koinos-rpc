export const tokenProto = {
   nested: {
      koinos: {
         nested: {
            contracts: {
               nested: {
                  token: {
                     nested: {
                        name_arguments: {
                           fields: {}
                        },
                        name_result: {
                           fields: {
                              value: {
                                 type: 'string',
                                 id: 1
                              }
                           }
                        },
                        symbol_arguments: {
                           fields: {}
                        },
                        symbol_result: {
                           fields: {
                              value: {
                                 type: 'string',
                                 id: 1
                              }
                           }
                        },
                        decimals_arguments: {
                           fields: {}
                        },
                        decimals_result: {
                           fields: {
                              value: {
                                 type: 'uint32',
                                 id: 1
                              }
                           }
                        },
                        total_supply_arguments: {
                           fields: {}
                        },
                        total_supply_result: {
                           fields: {
                              value: {
                                 type: 'uint64',
                                 id: 1,
                                 options: {
                                    jstype: 'JS_STRING'
                                 }
                              }
                           }
                        },
                        balance_of_arguments: {
                           fields: {
                              owner: {
                                 type: 'bytes',
                                 id: 1,
                                 options: {
                                    '(koinos.btype)': 'ADDRESS'
                                 }
                              }
                           }
                        },
                        balance_of_result: {
                           fields: {
                              value: {
                                 type: 'uint64',
                                 id: 1,
                                 options: {
                                    jstype: 'JS_STRING'
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }
};
