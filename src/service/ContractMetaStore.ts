import { Client } from '../Client';
import { MethodName } from '../types';

interface GetContractMetaResponse {
   meta: any;
}

export class ContractMetaStore {
   constructor(private readonly client: Client) {}

   async getContractMeta(contractId: string): Promise<GetContractMetaResponse> {
      return this.call('get_contract_meta', { contract_id: contractId });
   }

   private async call<T>(method: MethodName, params: any = {}): Promise<T> {
      return this.client.call('contract_meta_store', method, params);
   }
}
