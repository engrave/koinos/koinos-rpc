import { Client } from '../Client';
import { MethodName } from '../types';

export interface GetAccountHistoryResponse {
   values: any[];
}

interface GetAccountHistoryParams {
   address: string;
   seq_num: null | number;
   limit?: number;
   ascending?: boolean;
   irreversible?: boolean;
}

export class AccountHistory {
   constructor(private readonly client: Client) {}

   async getAccountHistory({ address, seq_num, limit = 50, ascending = false, irreversible = false }: GetAccountHistoryParams): Promise<GetAccountHistoryResponse> {
      return this.call('get_account_history', {
         address,
         seq_num,
         limit,
         ascending,
         irreversible
      });
   }

   private async call<T>(method: MethodName, params: any = {}): Promise<T> {
      return this.client.call('account_history', method, params);
   }
}
