import { Client } from '../Client';
import { Transaction } from '../types/Transaction';
import { MethodName } from '../types';

export interface GetTransactionsResponse {
   transactions: Transaction[];
}

export class TransactionStore {
   constructor(private readonly client: Client) {}

   async getTransactionsById(ids: string[]): Promise<GetTransactionsResponse> {
      return this.call('get_transactions_by_id', { transaction_ids: ids });
   }

   private async call<T>(method: MethodName, params: any = {}): Promise<T> {
      return this.client.call('transaction_store', method, params);
   }
}
