import { Client } from '../Client';
import { Block, BlockTopology } from '../types/Block';
import { MethodName } from '../types';

export interface GetBlocksResponse {
   block_items: Block[];
}

export interface GetHighestBlockResponse {
   topology: BlockTopology;
}

interface BlockchainStreamOptions {
   from?: number;
   to?: number;
}

export class BlockStore {
   constructor(private readonly client: Client) {}

   async getBlocksById(ids: string[], return_block = true, return_receipt = true): Promise<GetBlocksResponse> {
      return this.call('get_blocks_by_id', { block_ids: ids, return_block, return_receipt });
   }

   async getBlocksByHeight(head_block_id: string, ancestor_start_height: number, num_blocks: number, return_block = true, return_receipt = true): Promise<GetBlocksResponse> {
      return this.call('get_blocks_by_height', {
         head_block_id,
         ancestor_start_height,
         num_blocks,
         return_block,
         return_receipt
      });
   }

   async getHighestBlock(): Promise<GetHighestBlockResponse> {
      return this.call('get_highest_block');
   }

   async *getBlockNumbers(options?: BlockchainStreamOptions) {
      const interval = 3;
      if (!options) {
         options = {};
      }

      let current = await this.getHighetBlockNumber();
      if (options.from !== undefined && options.from > current) {
         throw new Error(`From can't be larger than current block num (${current})`);
      }
      let seen = options?.from !== undefined ? options.from : current;
      while (true) {
         while (current > seen) {
            yield seen++;
            if (options.to !== undefined && seen > options.to) {
               return;
            }
         }
         await this.sleep(interval * 1000);
         current = await this.getHighetBlockNumber();
      }
   }

   async *getBlocks(options?: BlockchainStreamOptions): AsyncIterable<Block> {
      for await (const num of this.getBlockNumbers(options)) {
         const { topology } = await this.getHighestBlock();
         const { block_items } = await this.getBlocksByHeight(topology.id, num, 1);
         yield block_items[0]!;
      }
   }

   private async getHighetBlockNumber() {
      const { topology } = await this.getHighestBlock();
      return topology.height;
   }

   private async sleep(number: number) {
      return new Promise((resolve) => setTimeout(resolve, number));
   }

   private async call<T>(method: MethodName, params: any = {}): Promise<T> {
      return this.client.call('block_store', method, params);
   }
}
