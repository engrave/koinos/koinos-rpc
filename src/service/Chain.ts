import { Client } from '../Client';
import { BlockTopology } from '../types/Block';
import { MethodName } from '../types';

interface HeadInfoResponse {
   head_topology: BlockTopology;
   last_irreversible_block: number;
   head_state_merkle_root: string; // base64;
   head_block_time: number;
}

interface GetChainIdResponse {
   chain_id: string;
}

interface GetForkHeadsResponse {
   last_irreversible_block: BlockTopology;
   fork_heads: BlockTopology[];
}

interface GetAccountNonceResponse {
   nonce: string; // base64url
}

interface GetAccountRcResponse {
   rc: string; // uint64
}

interface GetResourceLimitsResponse {
   resource_limit_data: {
      disk_storage_limit: string;
      disk_storage_cost: string;
      network_bandwidth_limit: string;
      network_bandwidth_cost: string;
      compute_bandwidth_limit: string;
      compute_bandwidth_cost: string;
   };
}

interface ReadContractResponse {
   result: any;
}

export class Chain {
   constructor(private readonly client: Client) {}

   async getHeadInfo(): Promise<HeadInfoResponse> {
      return this.call('get_head_info');
   }

   async getChainId(): Promise<GetChainIdResponse> {
      return this.call('get_chain_id');
   }

   async getForkHeads(): Promise<GetForkHeadsResponse> {
      return this.call('get_fork_heads');
   }

   async getAccountNonce(account: string): Promise<GetAccountNonceResponse> {
      return this.call('get_account_nonce', { account });
   }

   async getAccountRC(account: string): Promise<GetAccountRcResponse> {
      return this.call('get_account_rc', { account });
   }

   async getResourceLimits(): Promise<GetResourceLimitsResponse> {
      return this.call('get_resource_limits');
   }

   async readContract(contract_id: string, entry_point: number, args: any): Promise<ReadContractResponse> {
      return this.call('read_contract', { contract_id, entry_point, args });
   }

   private async call<T>(method: MethodName, params: any = {}): Promise<T> {
      return this.client.call('chain', method, params);
   }
}
