import { Operation, Transaction } from './Transaction';

export interface Event {
   sequence?: number;
   source: string;
   name: string;
   data: string;
   impacted: string[];
}

export interface BlockReceipt {
   id: string;
   height: string; // number
   network_bandwidth_used: string; // number
   compute_bandwidth_used: string; // number
   events: Event[];
}

export interface Block {
   block_id: string;
   block_height: string; // number
   block?: {
      id: string;
      header: {
         previous: string;
         height: string; // number
         timestamp: string; // number
         previous_state_merkle_root: string;
         transaction_merkle_root: string;
         signer: string; // account
      };
      transactions: Transaction[]; // TODO verify if it's the same as in TransactionStore
      operations: Operation[];
      signatures: string[];
   };
   receipt?: BlockReceipt;
}

export interface BlockTopology {
   id: string;
   height: number;
   previous: string;
}
