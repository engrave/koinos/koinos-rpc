export interface TransactionHeader {
   chain_id: string;
   rc_limit: string; // number
   nonce: string; // base64url
   operation_merkle_root: string;
   payer: string; // address
}

export interface Operation {
   call_contract?: {
      contract_id: string;
      entry_point: string;
      args: string;
   };
   upload_contract?: any; // TODO introduce type
   set_system_call?: any; // TODO introduce type
   set_system_contract?: any; // TODO introduce type
   // TODO more types
}

export interface Transaction {
   id: string;
   header: TransactionHeader;
   operations: Operation[];
   signatures: string[];
   containing_blocks: string[];
}

export type ContractMethodName = string;
export interface ContractMethod {
   argument: string;
   return: string;
   'entry-point': string; // number
   description: string;
   'read-only': boolean;
}

export interface Abi {
   methods: Record<ContractMethodName, ContractMethod>;
   types: string;
}
