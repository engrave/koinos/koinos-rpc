export * from './Block';
export * from './Transaction';

export type ServiceName = 'chain' | 'transaction_store' | 'block_store' | 'account_history';
export type MethodName = string;

export interface ServiceMethods extends Record<ServiceName, MethodName> {
   block_store: 'get_blocks_by_id' | 'get_blocks_by_height' | 'get_highest_block';
   chain: 'get_head_info' | 'get_chain_id' | 'get_fork_heads' | 'get_account_nonce' | 'get_account_rc' | 'get_resource_limits' | 'read_contract';
   contract_meta_store: 'get_contract_meta';
   transaction_store: 'get_transactions_by_id';
   account_history: 'get_account_history';
}
