import axios from 'axios';
import VError from 'verror';
import { Chain } from './service/Chain';
import { BlockStore } from './service/BlockStore';
import { TransactionStore } from './service/TransactionStore';
import { ContractMetaStore } from './service/ContractMetaStore';
import { ServiceMethods } from './types';
import { AccountHistory } from './service/AccountHistory';

interface ClientOptions {
   // TODO implement
   timeout?: number;
}

export class Client {
   public readonly chain: Chain;
   public readonly blockStore: BlockStore;
   public readonly transactionStore: TransactionStore;
   public readonly contractMetaStore: ContractMetaStore;

   public readonly accountHistory: AccountHistory;

   private readonly addresses: string[];
   private readonly currentAddress: string;
   private options: ClientOptions;

   constructor(address: string | string[], options: ClientOptions = {}) {
      this.addresses = Array.isArray(address) ? address : [address];
      this.currentAddress = this.addresses[0];
      this.options = options;

      this.chain = new Chain(this);
      this.blockStore = new BlockStore(this);
      this.transactionStore = new TransactionStore(this);
      this.contractMetaStore = new ContractMetaStore(this);
      this.accountHistory = new AccountHistory(this);
   }

   async call<Result, Service extends keyof ServiceMethods, Method = ServiceMethods[Service]>(service: Service, method: Method, params: any = {}): Promise<Result> {
      const { data } = await axios.post(this.currentAddress, {
         id: 1,
         jsonrpc: '2.0',
         method: `${service}.${method}`,
         params
      });

      if (data.result) {
         return data.result;
      }

      if (data.error) {
         throw this.formatError(data.error);
      }

      throw new Error('Unexpected error');
   }

   private formatError(error: any): VError {
      if (error instanceof VError) {
         return error;
      }
      return new VError(
         {
            name: 'RPCError',
            info: error
         },
         error?.message as string
      );
   }
}
