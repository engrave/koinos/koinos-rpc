import { expect } from 'chai';
import { Client } from '../../../src';
import { mainnetApi } from '../client.test';

describe('block_store service', () => {
   it('should return block by id', async () => {
      const client = new Client([mainnetApi]);
      const { block_items } = await client.blockStore.getBlocksById(['0x1220cbb1958e657107109ae40d92decc6b42bb7ddb05f064d20abdc1cf96c6cf98e5']);
      expect(block_items).to.have.length(1);
   });

   it('should return 5 latest blocks', async () => {
      const client = new Client([mainnetApi]);
      const { topology } = await client.blockStore.getHighestBlock();
      const { block_items } = await client.blockStore.getBlocksByHeight(topology.id, topology.height - 5, 5, true, false);
      expect(block_items).to.be.instanceof(Array).and.have.lengthOf(5);
   });

   it('should return highest block topology', async () => {
      const client = new Client([mainnetApi]);
      const { topology } = await client.blockStore.getHighestBlock();
      expect(topology).to.have.property('id');
      expect(topology).to.have.property('height');
      expect(topology).to.have.property('previous');
   });
});
