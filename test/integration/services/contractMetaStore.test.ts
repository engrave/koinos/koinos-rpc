import { expect } from 'chai';
import { Client } from '../../../src';
import { mainnetApi } from '../client.test';

describe('contract_meta_store service', () => {
   it('should return block by id', async () => {
      const client = new Client([mainnetApi]);
      const { meta } = await client.contractMetaStore.getContractMeta('15DJN4a8SgrbGhhGksSBASiSYjGnMU8dGL');
      expect(meta).to.have.property('abi');
   });
});
