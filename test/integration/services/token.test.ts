import { expect } from 'chai';
import { Client } from '../../../src';
import { mainnetApi } from '../client.test';
import { Token } from '../../../src/helpers/token';

describe('token helper', () => {
   it('should return token precision', async () => {
      const client = new Client([mainnetApi]);
      const token = await Token.create(client, '15DJN4a8SgrbGhhGksSBASiSYjGnMU8dGL'); // tKOIN
      expect(await token.getPrecision()).to.equal(8);
   });

   it('should return token symbol', async () => {
      const client = new Client([mainnetApi]);
      const token = await Token.create(client, '15DJN4a8SgrbGhhGksSBASiSYjGnMU8dGL'); // tKOIN
      expect(await token.getSymbol()).to.equal('KOIN');
   });
});
