import { expect } from 'chai';
import base64url from 'base64url';
import { Client } from '../../../src';
import { mainnetApi } from '../client.test';

describe('chain service', () => {
   it('should return head info', async () => {
      const client = new Client([mainnetApi]);
      await client.chain.getHeadInfo();
      // TODO expect
   });

   it('should return chainId', async () => {
      const client = new Client([mainnetApi]);
      const { chain_id } = await client.chain.getChainId();
      expect(chain_id).to.be.equal('EiBZK_GGVP0H_fXVAM3j6EAuz3-B-l3ejxRSewi7qIBfSA==');
      expect(base64url.toBuffer(chain_id)).to.have.length(34);
   });

   it('should return fork heads', async () => {
      const client = new Client([mainnetApi]);
      await client.chain.getForkHeads();
      // TODO expect
   });

   it('should return account nonce', async () => {
      const client = new Client([mainnetApi]);
      await client.chain.getAccountNonce('1NSk6yzaDHkqsmZhsahSULCXKgJMusW2We');
      // TODO expect
   });

   it('should get account rc', async () => {
      const client = new Client([mainnetApi]);
      await client.chain.getAccountRC('1NSk6yzaDHkqsmZhsahSULCXKgJMusW2We');
      // TODO expect
   });

   it('should get resource limits', async () => {
      const client = new Client([mainnetApi]);
      await client.chain.getResourceLimits();
      // TODO expect
   });

   it('should read contract data', async () => {
      const client = new Client([mainnetApi]);
      const result = await client.chain.readContract('15DJN4a8SgrbGhhGksSBASiSYjGnMU8dGL', 2191741823, '');
      expect(result).to.deep.equal({
         result: 'CgRLb2lu'
      });
   });
});
