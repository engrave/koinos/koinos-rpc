import { expect } from 'chai';
import { Client } from '../../../src';
import { mainnetApi } from '../client.test';

describe('transaction_store service', () => {
   it('should return transactions by id', async () => {
      const client = new Client([mainnetApi]);
      const { transactions } = await client.transactionStore.getTransactionsById(['0x1220b9b6e6127fe7f2643185ad9f2337922c92e5b8d21815d4f17ec77e825a58bd53']);
      expect(transactions).to.have.length(1);
   });
});
