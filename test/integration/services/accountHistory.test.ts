import { expect } from 'chai';
import { Client } from '../../../src';
import { mainnetApi } from '../client.test';

describe('account_history service', () => {
   it('should return account history', async () => {
      const client = new Client([mainnetApi]);
      const { values } = await client.accountHistory.getAccountHistory({
         address: '15DJN4a8SgrbGhhGksSBASiSYjGnMU8dGL',
         seq_num: null,
         limit: 11
      });
      expect(values).to.have.length(11);
   }).timeout(10000);
});
