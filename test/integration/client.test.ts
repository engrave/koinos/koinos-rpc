import { assert, expect } from 'chai';
import VError from 'verror';
import { Client } from '../../src';

export const mainnetApi = 'https://api.koinosblocks.com';

describe('Client test', () => {
   it('should return value on proper call', async () => {
      const client = new Client([mainnetApi]);
      await client.call('chain', 'get_head_info');
      // TODO expect
   });

   it('should return RPCError on improper call', async () => {
      try {
         const client = new Client([mainnetApi]);
         await client.call('block_store', 'get_blocks_by_id', {});
         assert(false, 'should not be reached');
      } catch (error) {
         expect(error).to.be.instanceof(VError);
         expect(error).to.have.property('name', 'RPCError');
         expect(error).to.have.property('jse_shortmsg', `expected field 'block_id' was nil`);
         expect(error).to.have.deep.property('jse_info', { code: -32603, message: "expected field 'block_id' was nil" });
      }
   });
});
